export enum EditorMode {
    HardLimits = 'hardLimits',
    SoftLimits = 'softLimits',
    Free = 'free',
    Default = SoftLimits
}
