export const sheet = {
    de: {
        weapon: {
            name: 'Waffen',
            handling: 'Handhabung',
            distance: 'Distanz',
            damage: 'Schaden',
            magazine: 'Magazin',
            properties: 'Eigenschaften',
            encumbrance: 'Last',
            tech: 'Tech',
            slots: 'Slots'
          },
          armor: {
            name: 'Rüstung',
            value: 'Panzerwert',
            properties: 'Eigenschaften',
            encumbrance: 'Last',
            tech: 'Tech',
            slots: 'Slots'
          }
    },
    en: {
        weapon: {
            name: 'Weapon name',
            handling: 'Handling',
            distance: 'Distance',
            damage: 'Damage',
            magazine: 'Mag',
            properties: 'Properties',
            encumbrance: 'Enc',
            tech: 'Tech',
            slots: 'Slots'
          },
          armor: {
            name: 'Armor',
            value: 'Value',
            properties: 'Properties',
            encumbrance: 'Enc',
            tech: 'Tech',
            slots: 'Slots'
          }
    },
}