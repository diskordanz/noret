export const clanNames = {
  de: {
    hunterGatherers: 'Jäger und Sammler',
    touloni: 'Touloni',
    sanglier: 'Sanglier',
    bordenoir: 'Bordenoir',
    resistance: 'Resistance',
    pneumancers: 'Pneumanten',
    exalters: 'Exalter',
  },
  en: {
    hunterGatherers: 'Hunter Gatherers',
    touloni: 'Touloni',
    sanglier: 'Sanglier',
    bordenoir: 'Bordenoir',
    resistance: 'Resistance',
    pneumancers: 'Pneumancers',
    exalters: 'Exalters',
  }
}
