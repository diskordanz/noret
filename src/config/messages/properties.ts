export const properties = {
  de: {
    attributes: {
      body: 'Körper',
      agility: 'Geschicklichkeit',
      charisma: 'Charisma',
      intellect: 'Intellekt',
      psyche: 'Psyche',
      instinct: 'Instinkt'
    },
    skills: {
      athletics: 'Athletik',
      brawl: 'Faustkampf',
      force: 'Kraft',
      melee: 'Nahkampf',
      stamina: 'Ausdauer',
      toughness: 'Härte',

      crafting: 'Handwerk',
      dexterity: 'Fingerfertigkeit',
      navigation: 'Steuern',
      mobility: 'Beweglichkeit',
      projectiles: 'Schusswaffen',
      stealth: 'Heimlichkeit',

      arts: 'Künste',
      conduct: 'Verhalten',
      expression: 'Ausdruck',
      leadership: 'Führung',
      negotiation: 'Verhandlung',
      seduction: 'Verführung',

      artifactLore: 'Artefaktkunde',
      engineering: 'Technik',
      focus: 'Fokus',
      legends: 'Legenden',
      medicine: 'Medizin',
      science: 'Wissenschaft',

      cunning: 'Gerissenheit',
      deception: 'Täuschung',
      domination: 'Dominieren',
      faith: 'Glaube',
      reaction: 'Reaktion',
      willpower: 'Wille',

      empathy: 'Empathie',
      orienteering: 'Orientierung',
      perception: 'Wahrnehmung',
      primal: 'Primal',
      survival: 'Überleben',
      taming: 'Zähmen'
    },
    origins: {
      allies: 'Verbündete',
      authority: 'Autorität',
      network: 'Netzwerk',
      renown: 'Ruf',
      resources: 'Ressourcen',
      secrets: 'Geheimnisse'
    }
  },
  en: {
    attributes: {
      body: 'Body',
      agility: 'Agility',
      charisma: 'Charisma',
      intellect: 'Intellect',
      psyche: 'Psyche',
      instinct: 'Instinct'
    },
    skills: {
      athletics: 'Athletics',
      brawl: 'Brawl',
      force: 'Force',
      melee: 'Melee',
      stamina: 'Stamina',
      toughness: 'Toughness',

      crafting: 'Crafting',
      dexterity: 'Dexterity',
      navigation: 'Navigation',
      mobility: 'Mobility',
      projectiles: 'Projectiles',
      stealth: 'Stealth',

      arts: 'Arts',
      conduct: 'Conduct',
      expression: 'Expression',
      leadership: 'Leadership',
      negotiation: 'Negotiation',
      seduction: 'Seduction',

      artifactLore: 'Artifact Lore',
      engineering: 'Engineering',
      focus: 'Focus',
      legends: 'Legends',
      medicine: 'Medicine',
      science: 'Science',

      cunning: 'Cunning',
      deception: 'Deception',
      domination: 'Domination',
      faith: 'Faith',
      reaction: 'Reaction',
      willpower: 'Willpower',

      empathy: 'Empathy',
      orienteering: 'Orienteering',
      perception: 'Perception',
      primal: 'Primal',
      survival: 'Survival',
      taming: 'Taming'
    },
    origins: {
      allies: 'Allies',
      authority: 'Authority',
      network: 'Network',
      renown: 'Renown',
      resources: 'Resources',
      secrets: 'Secrets'
    }
  }
}
